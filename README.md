
But copier automatiquement (et optionnelement virer les autres) sa clef publique
sur toutes les machines dès qu'on se connecte en ssh.

# Avertissement

Toute commande ou configuration doit être intégralement comprise avant d'être
utilisée. De manière générale et encore plus lorsqu'on touche à la sécurité.

# Multiplexage

On a besoin du multiplexage de connexion ssh (car on se connecte en parallele
pour chaque nouvelle connexion principale).

Créer le dossier `~/.ssh/controlmasters`, dans le `~/.ssh/config`, à la fin :

```
Host *
    ControlMaster auto
    ControlPath ~/.ssh/controlmasters/%C
    ControlPersist 5s
```

Je vire la connexion 5s après la dernière déconnexion du mux. Ce qui permet que
les programmes qui utilisent des connexions à suivre (au hasard, darcs) soient
dans le même mux.

# Local Command 

Tout est basé sur une local command executé par ssh après l'établissement de la
connexion.

Mettre dans le `~/.ssh/config`, à la fin:

```
Host *
    PermitLocalCommand yes
    LocalCommand $HOME/.ssh/manage_my_keys %C %r %h %p
```

Ce script exécute `~/.ssh/manage_my_keys`, en fait il attend 3 secondes (le
temps que le mux soit accessible), et il exécute le script
`~/.ssh/manage_my_keys_remote` sur le serveur (après l'avoir envoyé via la
cmdline en base64).

# Adaptation

Il faut adapter le script `~/.ssh/manage_my_keys_remote` pour mettre la bonne
clef publique, et savoir si on écrase les autres ou non.

# Usage

Si on veut ne jamais le faire pour un serveur, ajouter dans le `~/.ssh/config`,
avant les `Host *`:

```
Host a_server_I_dont_want_copy_my_pub_id
    PermitLocalCommand no
```

Si on veut ponctuellement ne pas le faire, utiliser la commande:

```
ssh -oPermitLocalCommand=no
```
